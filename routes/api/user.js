const express = require('express');
const router = express.Router();
const userController = require('../../controllers/userController')

router.route('/register').post(userController.handleNewUser)
router.route('/login').post(userController.handleLogin)
    

module.exports = router;