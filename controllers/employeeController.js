const data = {
    employees : require('../models/employees.json'),
    setEmployees : function (data) {
        this.employees = data
    }
};

const getAllEmployees = (req, res) => {
    res.json(data.employees);
}

const storeEmployee = (req, res) => {
    // buat instance employee baru,
    const newEmployee = {
        id: data.employees[data.employees.length - 1].id + 1 || 1,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
    }

    // validation required input first name and last name
    if (!newEmployee.firstname || !newEmployee.lastname) {
        return res.status(400).json({ 'message' : 'first name and last name are required'});
    }

    data.setEmployees([...data.employees, newEmployee]);
    res.status(201).json(data.employees);
}

const getEmployee = (req, res) => {
    // semisal cari di database dgn pk id
    const employee = data.employees.find(emp => emp.id == parseInt(req.body.id));
    // kalau ga ketemu return error
    if (!employee) {
        return res.status(400).json({ 'message': `Employee ID ${req.body.id} not found`});
    }
    res.json(employee);
}

const updateEmployee = (req, res) => {
    // semisal cari di database dgn pk id
    const employee = data.employees.find(emp => emp.id == parseInt(req.body.id));
    // kalau ga ketemu return error
    if (!employee) {
        return res.status(400).json({ 'message': `Employee ID ${req.body.id} not found`});
    }
    // update kolomnya
    if (req.body.firstname) employee.firstname = req.body.firstname;
    if (req.body.lastname) employee.lastname = req.body.lastname;
    // ambil baris tanpa employee, kemudian taruh employee ke array tersebut di akhir element, lalu urutkan. dgn asusmi ID adalah auto increment
    const filteredArray = data.employees.filter(emp => emp.id !== parseInt(req.body.id));
    const unsortedArray = [...filteredArray, employee];
    data.setEmployees(unsortedArray.sort((a, b) => a.id > b.id ? 1 : a.id < b.id ? -1 : 0 ))
    res.json(data.employees);
}

const deleteEmployee = (req, res) => {
    // semisal cari di database dgn pk id
    const employee = data.employees.find(emp => emp.id == parseInt(req.body.id));
    // kalau ga ketemu return error
    if (!employee) {
        return res.status(400).json({ 'message': `Employee ID ${req.body.id} not found`});
    }
    // eliminasi array
    const filteredArray = data.employees.filter(emp => emp.id !== parseInt(req.body.id));
    data.setEmployees([...filteredArray]);
    res.json(data.employees);
}

module.exports = {
    getAllEmployees,
    storeEmployee,
    getEmployee,
    updateEmployee,
    deleteEmployee
}