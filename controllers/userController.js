const usersDB = {
    users: require('../models/users.json'),
    setUsers: function (data) {
        this.users = data
    }
}

const fsPromises = require('fs').promises;
const path = require('path');
const bcrypt = require('bcrypt'); // enkripsi password

const handleNewUser = async (req, res) => {
    const { username, password } = req.body;
    // validasi required input
    if (!username || !password) return res.status(400).json({ 'message' : 'Username and password are required' });
    
    // validasi username exist
    const duplicate = usersDB.users.find(user => user.username === username);
    if (duplicate) return res.sendStatus(409); // conflict


    try {
        // encrypt the password, 10 adalah rand 10 kali
        const hashedPassword = await bcrypt.hash(password, 10);
        // store user
        const newUser = { "username": username, "password": hashedPassword };
        usersDB.setUsers([...usersDB.users, newUser]);
        await fsPromises.writeFile(
            path.join(__dirname, '..', 'models', 'users.json'),
            JSON.stringify(usersDB.users)
        );
        console.log(usersDB.users);
        res.status(201).json({ 'message' : `New user ${newUser} created`});
    } catch (error) {
        res.status(500).json({ 'message': error.message });
    }
}

const handleLogin = async (req, res) => {
    const { username, password } = req.body;
    // validasi required input
    if (!username || !password) return res.status(400).json({ 'message' : 'Username and password are required' });

    // is user exist
    const foundUser = usersDB.users.find(user => user.username === username);
    if (!foundUser) return  res.sendStatus(401) //unauthorized
    // evaluate password
    const matchPassword = await bcrypt.compare(password, foundUser.password);
    if (matchPassword) {
        res.json({ 'message' : `User ${foundUser} is logged in!`})
    } else {
        res.sendStatus(401);
    }
}

module.exports = {
    handleNewUser,
    handleLogin
}